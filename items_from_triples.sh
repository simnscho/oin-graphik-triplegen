#!/bin/bash

# reads a ntriples file from stdin.
# outputs  bv number and signature, one pair per line

grep -oP '/item-BV\d+-[^->]+' |sort -u | cut -c 7- | sed 's!%20! !g;s!%28!(!g;s!%2C!,!g;s!%2F!/!g;s!%5B![!g;s!-!\t!'
