#!/bin/bash

#rm data/cct/*.rdf

# read BV IDs from stdin, one per line
c=0
for bvid in $(cat -); do
  ((c++))
  if [ "x$bvid" != "x" ]; then
    echo "read $c lines; process id $bvid"
    java -jar /usr/share/java/Saxon-HE.jar -s:data/xfm/$bvid.xml -xsl:compile_to_triples.xsl |
    xmlstarlet fo | 
    tee data/ctt/$bvid.hash.xml |
    # convert <hash> tags to md5 hashes
    php -r '
      while ($l = fgets(STDIN)) {
        # replace sequence @@@hashNN:XXX:hsah@@@ with first NN characters of the md5sum of XXX
        $l = preg_replace_callback('\''!@@@hash(\d+):([^<]*?):hsah@@@!u'\'', function($matches) {
          return substr(md5(html_entity_decode($matches[2], ENT_XML1, "UTF-8")), 0, $matches[1]);
        }, $l);
        # replace sequence @@@escapeUri:XXX:epacse@@@ with URI encoded version of string XXX
        $l = preg_replace_callback('\''!@@@escapeUri:([^<]*?):epacse@@@!u'\'', function($matches) {
          return rawurlencode($matches[1]);
        }, $l);
        echo $l;
      }' > data/ctt/$bvid.rdf
  fi
done
  
