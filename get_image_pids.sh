#!/bin/bash

for bvid in $(cat -); do
  pid=`echo "$bvid" | ./extract_work_pids.sh`
  echo "Get pids for $bvid with pid $pid..."
  if [[ "x$pid" != "x" ]]; then 
    curl "http://digital.bib-bvb.de/view/bvbmets/getStructMap.jsp?pid=$pid&parta=DE-29&au=BAY01" |
    # the structmap is arranged such that always the first children array item has the right image!
    jq -j '"<pid>",.[0].children[0].attr.pid,"</pid>"' > data/pid/$bvid.xml
  else 
    echo "<pid></pid>" > data/pid/$bvid.xml
  fi
done
