#!/usr/bin/perl

$ecrm = "http://erlangen-crm.org/170309";
$oin = "http://objekte-im-netz.fau.de/ontology/common";
$grasa = "http://objekte-im-netz.fau.de/ontology/graphik";
$hgs = "http://id.ub.fau.de/hgs/ontology";
$type = "http://id.ub.fau.de/hgs/types/";
$inst = "http://id.ub.fau.de/hgs/inst";
$owl = "http://www.w3.org/2002/07/owl";
$a = "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>";

while(<>) {
  s/"?\|"?/|/g;
  @line = split /\|/;
  print STDERR "bad line: '$_'\n" and next unless scalar @line == 8;
  ($kurztitel, $titel, $band, $digi, $jahr, $ort, $autor, $norm_uri) = @line;
  chomp $norm_uri;
  
  $uri = $kurztitel;
  $uri =~ s/[^-a-zA-Z_0-9]+/_/g;
  $lituri = "$inst/lit-$uri";

  print   "<$lituri> $a <$oin/S87_Literature> .\n";
  print   "<$lituri> <$ecrm/P48_has_preferred_identifier> <$lituri-id> .\n";
  print   "<$lituri-id> $a <$oin/S100_Reference_Title> .\n";
  print   "<$lituri-id> <$ecrm/P3_has_note> " . escape($kurztitel) . " .\n";
  print   "<$lituri> <$ecrm/P102_has_title> <$lituri-title> .\n";
  print   "<$lituri-title> $a <$ecrm/E35_Title> .\n";
  print   "<$lituri-title> <$ecrm/P3_has_note> " . escape($titel) . " .\n";
  if ($band ne '') {
    print "<$lituri> <$ecrm/P1_is_identified_by> <$lituri-volume> .\n";
    print "<$lituri-volume> $a <$grasa/Volume_Number> .\n";
    print "<$lituri-volume> <$ecrm/P3_has_note> " . escape($band) . " .\n";
  }
  if ($digi ne '') {
    print "<$lituri> <$ecrm/P1_is_identified_by> <$lituri-digitized> .\n";
    print "<$lituri> <$oin/N40_has_URL> <$lituri-digitized> .\n";
    print "<$lituri-digitized> $a <$ecrm/E51_Contact_Point> .\n";
    print "<$lituri-digitized> $a <$oin/S65_URL> .\n";
    print "<$lituri-digitized> <$ecrm/P3_has_note> " . escape($digi) . " .\n";
  }
  print   "<$lituri> <$ecrm/P92i_was_brought_into_existence_by> <$lituri-creation> .\n";
  print   "<$lituri-creation> $a <$ecrm/E65_Creation> .\n";
  if ($jahr ne '') {
    print "<$lituri-creation> <$ecrm/P4_has_time-span> <$lituri-date> .\n";
    print "<$lituri-date> $a <$ecrm/E52_Time-Span> .\n";
    print "<$lituri-date> <$ecrm/P78_is_identified_by> <$lituri-datelabel> .\n";
    print "<$lituri-datelabel> $a <$ecrm/E49_Time_Appellation> .\n";
    print "<$lituri-datelabel> <$ecrm/P3_has_note> " . escape($jahr) . " .\n";
  }
  if ($ort ne '') {
    $placeuri = $ort;
    $placeuri =~ s/[^-a-zA-Z_0-9]+/_/g;
    $placeuri = "$inst/place-$placeuri";
    print "<$lituri-creation> <$ecrm/P7_took_place_at> <$placeuri> .\n";
    print "<$placeuri> $a <$oin/S40_Geographical_Place> .\n";
    print "<$placeuri> <$ecrm/P87_is_identified_by> <$placeuri-label> .\n";
    print "<$placeuri-label> $a <$ecrm/E48_Place_Name> .\n";
    print "<$placeuri-label> <$ecrm/P3_has_note> " . escape($ort) . " .\n";
  }
  if ($autor ne '') {
    $persuri = $autor;
    $persuri =~ s/[^-a-zA-Z_0-9]+/_/g;
    $persuri = "$inst/person-$persuri";
    print "<$lituri-creation> <$ecrm/P14_carried_out_by> <$persuri> .\n";
    print "<$persuri> $a <$oin/S89_Author> .\n";
    print "<$persuri> <$ecrm/P1_is_identified_by> <$persuri-name> .\n";
    print "<$persuri-name> $a <$ecrm/E41_Appellation> .\n";
    print "<$persuri-name> <$ecrm/P3_has_note> " . escape($autor) . " .\n";
    $gnd = person2gnd($autor);
    if ($gnd ne '') {
      print "<$persuri> <$owl#sameAs> <http://d-nb.info/gnd/$gnd> .\n";
    }
  }
  if ($norm_uri ne '') {
    print "<$lituri> <$owl#sameAs> <$norm_uri> .\n";
  }
  print "\n";
}

sub escape {
  $_ = shift @_;
  s/"/\\"/g;
  return '"' . $_ . '"';
}

sub person2gnd {
  $_ = shift @_;
  return '155764683' if /Kessler-Luhde/;
  return '116989440' if /Meder/;
  return '119320207' if /Bartsch/;
  return '118690108' if /Geisberg/;
  return '161107788' if /Strauss/;
  return '116309865' if /Andresen/;
  return '118710168' if /Heller/;
}
