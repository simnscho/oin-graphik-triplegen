#!/bin/bash

rm data/rdf/*.rdf

# read BV IDs from stdin, one per line
c=0
while read -r bvid; do
  ((c++))
  if [ "x$bvid" != "x" ]; then
    echo "read $c lines; process id $bvid"
    java -jar /usr/share/java/Saxon-HE.jar -s:dummy.xml -xsl:verbund2.xsl vdoc=data/source.b3kat/$bvid.xml ldoc=data/source.local/$bvid.xml | xmlstarlet fo > data/rdf/$bvid.rdf
  fi
done
  
