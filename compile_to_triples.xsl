<?xml version="1.0" encoding="utf-8"?>

<!DOCTYPE bibtrip [
<!ENTITY ecrm "http://erlangen-crm.org/170309/">
<!ENTITY samm "http://objekte-im-netz.fau.de/ontology/common/">
<!ENTITY grasa "http://objekte-im-netz.fau.de/ontology/graphik/">
<!ENTITY hgs "http://id.ub.fau.de/hgs/ontology/">
<!ENTITY type "http://id.ub.fau.de/hgs/types/">
<!ENTITY inst "http://id.ub.fau.de/hgs/inst/">
]>


<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema"
  xmlns:ctt="http://id.ub.fau.de/bibtrip/compile-to-triples"

  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:owl="http://www.w3.org/2002/07/owl#"
  xmlns:ecrm="http://erlangen-crm.org/170309/"
  xmlns:samm="http://objekte-im-netz.fau.de/ontology/common/"
  xmlns:grasa="http://objekte-im-netz.fau.de/ontology/graphik/"
  xmlns:hgs="http://id.ub.fau.de/hgs/ontology/"
  xmlns:type="http://id.ub.fau.de/hgs/types/"
  xmlns:inst="http://id.ub.fau.de/hgs/inst/"
>

<!-- Transforms a pair of MARC files that describe an expression/work and corresponding items.
     Requires the pair of files as parameters "vdoc" and "ldoc".
-->


<xsl:param name="pidPathPrefix" select="'data/pid/'" />
<xsl:param name="pidPathSuffix" select="'.xml'" />


<xsl:output method="xml" encoding="utf8" />

<xsl:template match="/">
  <rdf:RDF>
    <xsl:for-each select="/record">
      <xsl:choose>
        <xsl:when test="count(itemList/item) = 0">
          <xsl:message>skipping record with id <xsl:value-of select="work/bvid" />: no items</xsl:message>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="work" />
          <xsl:apply-templates select="itemList/item" />
          <xsl:apply-templates select="literatureList/literature" />
          <xsl:apply-templates select="personList/person" />
          <xsl:apply-templates select="sequel" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </rdf:RDF>
</xsl:template>


<xsl:template match="work">
  <xsl:param name="uri" select="concat('&inst;work-', bvid)" />
  <ecrm:E73_Information_Object>
    <xsl:attribute name="rdf:about">
      <xsl:value-of select="$uri"/>
    </xsl:attribute>
    <ecrm:P2_has_type>
      <ecrm:E55_Type rdf:about="&type;Collection_Object_Content" />
    </ecrm:P2_has_type>
    <ecrm:P48_has_preferred_identifier>
      <grasa:BV-ID>
        <xsl:attribute name="rdf:about">
          <xsl:value-of select="concat($uri, '-id')" />
        </xsl:attribute>
        <ecrm:P3_has_note>
          <xsl:value-of select="bvid" />
        </ecrm:P3_has_note>
      </grasa:BV-ID>
    </ecrm:P48_has_preferred_identifier>
    <ecrm:P1_is_identified_by>
      <xsl:attribute name="rdf:resource">
        <xsl:value-of select="concat($uri, '-id')" />
      </xsl:attribute>
    </ecrm:P1_is_identified_by>
    <ecrm:P102_has_title>
      <ecrm:E35_Title>
        <xsl:attribute name="rdf:about">
          <xsl:value-of select="concat($uri, '-title')" />
        </xsl:attribute>
        <ecrm:P3_has_note>
          <xsl:value-of select="title" />
        </ecrm:P3_has_note>
      </ecrm:E35_Title>
    </ecrm:P102_has_title>
    <xsl:apply-templates select="authority" />
    <ecrm:P94i_was_created_by>
      <ecrm:E65_Creation>
        <xsl:attribute name="rdf:about">
          <xsl:value-of select="concat($uri, '-creation')" />
        </xsl:attribute>
        <xsl:for-each select="production/date">
          <ecrm:P4_has_time-span>
            <xsl:call-template name="ctt:date" />
          </ecrm:P4_has_time-span>
        </xsl:for-each>
      </ecrm:E65_Creation>
    </ecrm:P94i_was_created_by>
    <xsl:call-template name="ctt:digitized-image">
      <xsl:with-param name="bvid" select="bvid" />
    </xsl:call-template>
  </ecrm:E73_Information_Object>
</xsl:template>


<xsl:template name="ctt:digitized-image">
  <xsl:param name="bvid" select="work/bvid" />
  <xsl:variable name="pid" select="document(concat($pidPathPrefix, $bvid, $pidPathSuffix))" />
  <xsl:for-each select="$pid/pid[. != '']">
    <xsl:variable name="url" select="concat('http://digital.bib-bvb.de/webclient/DeliveryManager?custom_att_2=download&amp;pid=', current(), '')" />
    <ecrm:P138i_has_representation>
      <samm:S21_Image>
        <xsl:attribute name="rdf:about">
          <xsl:value-of select="concat('&inst;image-', $bvid, '-', current())"/>
        </xsl:attribute>
        <samm:N40_has_URL>
          <xsl:call-template name="ctt:compileUrl">
            <xsl:with-param name="url" select="$url" />
          </xsl:call-template>
        </samm:N40_has_URL>
      </samm:S21_Image>
    </ecrm:P138i_has_representation>
  </xsl:for-each>
</xsl:template>

  
<xsl:template name="ctt:date">
  <xsl:param name="elem" select="." />
  <xsl:param name="uri" select="concat('&inst;timespan-@@@hash11:', $elem/label, ':hsah@@@')" />
  <ecrm:E52_Time-Span>
    <xsl:attribute name="rdf:about">
      <xsl:value-of select="$uri" />
    </xsl:attribute>
    <ecrm:P78_is_identified_by>
      <ecrm:E49_Time_Appellation>
        <xsl:attribute name="rdf:about">
          <xsl:value-of select="concat($uri, '-label')" />
        </xsl:attribute>
        <ecrm:P3_has_note>
          <xsl:value-of select="$elem/label" />
        </ecrm:P3_has_note>
      </ecrm:E49_Time_Appellation>
    </ecrm:P78_is_identified_by>
    <xsl:if test="string($elem/earliestStart) != ''">
      <samm:P82a_has_earliest_start>
        <xsl:value-of select="$elem/earliestStart" />
      </samm:P82a_has_earliest_start>
    </xsl:if>
    <xsl:if test="string($elem/latestStart) != ''">
      <samm:P81a_has_latest_start>
        <xsl:value-of select="$elem/latestStart" />
      </samm:P81a_has_latest_start>
    </xsl:if>
    <xsl:if test="string($elem/earliestEnd) != ''">
      <samm:P81b_has_earliest_end>
        <xsl:value-of select="$elem/earliestEnd" />
      </samm:P81b_has_earliest_end>
    </xsl:if>
    <xsl:if test="string($elem/latestEnd) != ''">
      <samm:P82b_has_latest_end>
        <xsl:value-of select="$elem/latestEnd" />
      </samm:P82b_has_latest_end>
    </xsl:if>
  </ecrm:E52_Time-Span>
</xsl:template>


<xsl:template match="authority">
  <xsl:param name="auth" select="." />
  <samm:N41_has_authority_file_id>
    <samm:S67_Authority_File_ID>
      <xsl:attribute name="rdf:about">
        <xsl:value-of select="concat('&inst;authentry-@@@hash11:', $auth/id, ':hsah@@@')" />
      </xsl:attribute>
      <ecrm:P3_has_note>
        <xsl:value-of select="$auth/entry/id" />
      </ecrm:P3_has_note>
      <xsl:if test="$auth/file">
        <ecrm:P71i_is_listed_in>
          <samm:S68_Authority_File>
            <xsl:variable name="fileUri" select="concat('&inst;authfile-', $auth/file/id)" />
            <xsl:attribute name="rdf:about">
              <xsl:value-of select="$fileUri" />
            </xsl:attribute>
            <xsl:if test="$auth/file/label">
              <ecrm:P102_has_title>
                <ecrm:E35_Title>
                  <xsl:attribute name="rdf:about">
                    <xsl:value-of select="concat($fileUri, '-title')" />
                  </xsl:attribute>
                  <ecrm:P3_has_note>
                    <xsl:value-of select="$auth/file/label" />
                  </ecrm:P3_has_note>
                </ecrm:E35_Title>
              </ecrm:P102_has_title>
            </xsl:if>
          </samm:S68_Authority_File>
        </ecrm:P71i_is_listed_in>
      </xsl:if>
      <xsl:for-each select="$auth/entry/uri">
        <ecrm:P139_has_alternative_form>
          <xsl:call-template name="ctt:compileUrl" />
        </ecrm:P139_has_alternative_form>
      </xsl:for-each>
    </samm:S67_Authority_File_ID>
  </samm:N41_has_authority_file_id>
  <xsl:if test="$auth/entry/uri">
    <owl:sameAs>
      <xsl:attribute name="rdf:resource">
        <xsl:value-of select="$auth/entry/uri" />
      </xsl:attribute>
    </owl:sameAs>
  </xsl:if>
</xsl:template>


<xsl:template name="ctt:compileUrl">
  <xsl:param name="url" select="." />
  <samm:S65_URL>
    <xsl:attribute name="rdf:about">
      <xsl:value-of select="concat('&inst;url-@@@hash11:', $url, ':hsah@@@')" />
    </xsl:attribute>
    <ecrm:P3_has_note>
      <xsl:value-of select="$url" />
    </ecrm:P3_has_note>
  </samm:S65_URL>
</xsl:template>


<xsl:template name="createItemUri">
  <xsl:param name="bvid" />
  <xsl:param name="shelfMark" />
  <xsl:value-of select="concat('&inst;item-', $bvid, '-@@@escapeUri:', $shelfMark, ':epacse@@@')" />
</xsl:template>

<xsl:template match="item">
  <xsl:variable name="bvid" select="../../work/bvid" />
  <xsl:variable name="uri">
    <xsl:call-template name="createItemUri">
      <xsl:with-param name="bvid" select="$bvid" />
      <xsl:with-param name="shelfMark" select="shelfMark" />
    </xsl:call-template>
  </xsl:variable>
  <samm:S1_Collection_Object>
    <xsl:attribute name="rdf:about">
      <xsl:value-of select="$uri" />
    </xsl:attribute>
    <rdf:type rdf:resource="&ecrm;E22_Man-Made_Object" />
    <ecrm:P2_has_type rdf:resource="&type;objtype-graphics" />
    <ecrm:P2_has_type rdf:resource="&type;Collection_Object" />
    <ecrm:P1_is_identified_by>
      <grasa:BV-ID>
        <xsl:attribute name="rdf:about">
          <xsl:value-of select="concat($uri, '-id')" />
        </xsl:attribute>
        <ecrm:P3_has_note>
          <xsl:value-of select="$bvid" />
        </ecrm:P3_has_note>
      </grasa:BV-ID>
    </ecrm:P1_is_identified_by>
    <ecrm:P48_has_preferred_identifier>
      <samm:S3_Inventory_Number>
        <xsl:attribute name="rdf:about">
          <xsl:value-of select="concat($uri, '-shelfmark')"/>
        </xsl:attribute>
        <ecrm:P3_has_note>
          <xsl:value-of select="shelfMark" />
        </ecrm:P3_has_note>
      </samm:S3_Inventory_Number>
    </ecrm:P48_has_preferred_identifier>
    <ecrm:P102_has_title>
      <ecrm:E35_Title>
        <xsl:attribute name="rdf:about">
          <xsl:value-of select="concat($uri, '-title')" />
        </xsl:attribute>
        <ecrm:P3_has_note>
          <xsl:value-of select="../../work/title" />
        </ecrm:P3_has_note>
      </ecrm:E35_Title>
    </ecrm:P102_has_title>
    <xsl:for-each select="carries">
      <ecrm:P128_carries>
        <xsl:attribute name="rdf:resource">
          <xsl:value-of select="concat('&inst;work-', .)"/>
        </xsl:attribute>
      </ecrm:P128_carries>
    </xsl:for-each>
    <ecrm:P108i_was_produced_by>
      <ecrm:E12_Production>
        <xsl:attribute name="rdf:about">
          <xsl:value-of select="concat($uri, '-production')" />
        </xsl:attribute>
        <!-- technique -->
        <xsl:for-each select="technique">
          <xsl:choose>
            <xsl:when test="id">
              <ecrm:P33_used_specific_technique>
                <xsl:attribute name="rdf:resource">
                  <xsl:value-of select="concat('&inst;technique-', id)" />
                </xsl:attribute>
              </ecrm:P33_used_specific_technique>
            </xsl:when>
            <xsl:otherwise>
              <ecrm:P33_used_specific_technique>
                <ecrm:E29_Design_or_Procedure>
                  <xsl:variable name="techUri">
                    <xsl:value-of select="concat('&inst;technique-@@@hash11:', ., ':hsah@@@')" />
                  </xsl:variable>
                  <xsl:attribute name="rdf:about">
                    <xsl:value-of select="$techUri" />
                  </xsl:attribute>
                  <ecrm:P149_is_identified_by>
                    <ecrm:E75_Conceptual_Object_Appellation>
                      <xsl:attribute name="rdf:about">
                        <xsl:value-of select="concat($techUri, '-title')" />
                      </xsl:attribute>
                      <rdf:type rdf:resource="ecrm:E41_Appellation" />
                      <ecrm:P3_has_note>
                        <xsl:value-of select="." />
                      </ecrm:P3_has_note>
                    </ecrm:E75_Conceptual_Object_Appellation>
                  </ecrm:P149_is_identified_by>
                  <ecrm:P1_is_identified_by>
                    <xsl:attribute name="rdf:resource">
                      <xsl:value-of select="concat($techUri, '-title')" />
                    </xsl:attribute>
                  </ecrm:P1_is_identified_by>
                </ecrm:E29_Design_or_Procedure>
              </ecrm:P33_used_specific_technique>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each><!-- technique -->
        <!-- material -->
        <xsl:for-each select="material">
          <xsl:choose>
            <xsl:when test="id">
              <ecrm:P32_used_general_technique>
                <xsl:attribute name="rdf:resource">
                  <xsl:value-of select="concat('&type;material-', id)" />
                </xsl:attribute>
              </ecrm:P32_used_general_technique>
              <ecrm:P126_employed>
                <xsl:attribute name="rdf:resource">
                  <xsl:value-of select="concat('&type;material-', id)" />
                </xsl:attribute>
              </ecrm:P126_employed>
            </xsl:when>
            <xsl:otherwise>
              <xsl:variable name="matUri">
                <xsl:value-of select="concat('&inst;material-@@@hash11:', ., ':hsah@@@')" />
              </xsl:variable>
              <ecrm:P32_used_general_technique>
                <ecrm:E57_Material>
                  <xsl:attribute name="rdf:about">
                    <xsl:value-of select="$matUri" />
                  </xsl:attribute>
                  <ecrm:P149_is_identified_by>
                    <ecrm:E75_Conceptual_Object_Appellation>
                      <xsl:attribute name="rdf:about">
                        <xsl:value-of select="concat($matUri, '-title')" />
                      </xsl:attribute>
                      <rdf:type rdf:resource="ecrm:E41_Appellation" />
                      <ecrm:P3_has_note>
                        <xsl:value-of select="." />
                      </ecrm:P3_has_note>
                    </ecrm:E75_Conceptual_Object_Appellation>
                  </ecrm:P149_is_identified_by>
                  <ecrm:P1_is_identified_by>
                    <xsl:attribute name="rdf:resource">
                      <xsl:value-of select="concat($matUri, '-title')" />
                    </xsl:attribute>
                  </ecrm:P1_is_identified_by>
                </ecrm:E57_Material>
              </ecrm:P32_used_general_technique>
              <ecrm:P126_employed>
                <xsl:attribute name="rdf:resource">
                  <xsl:value-of select="$matUri" />
                </xsl:attribute>
              </ecrm:P126_employed>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each><!-- end material -->
        <!-- date -->
        <xsl:for-each select="creation/date">
          <ecrm:P4_has_time-span>
            <xsl:call-template name="ctt:date" />
          </ecrm:P4_has_time-span>
        </xsl:for-each>
      </ecrm:E12_Production>
    </ecrm:P108i_was_produced_by>
    <!-- dimensions / measurements -->
    <xsl:for-each select="dimensions">
      <xsl:variable name="measureUri" select="concat('&inst;measurement-@@@hash11:', string-join(./*, '::'), ':hsah@@@')" /> 
      <ecrm:P39i_was_measured_by>
        <ecrm:E16_Measurement>
          <xsl:attribute name="rdf:about">
            <xsl:value-of select="$measureUri" />
          </xsl:attribute>
          <xsl:for-each select="diameter|height|width">
            <ecrm:P40_observed_dimension>
              <ecrm:E54_Dimension>
                <xsl:attribute name="rdf:about">
                  <xsl:value-of select="concat('&inst;dim-@@@hash11:', local-name(), '::', ., '::cm:hsah@@@')" />
                </xsl:attribute>
                <ecrm:P90_has_value>
                  <xsl:value-of select="." />
                </ecrm:P90_has_value>
                <ecrm:P2_has_type>
                  <ecrm:E55_Type>
                    <xsl:attribute name="rdf:about">
                      <xsl:value-of select="concat('&type;dimtype-', local-name())" />
                    </xsl:attribute>
                  </ecrm:E55_Type>
                </ecrm:P2_has_type>
                <ecrm:P91_has_unit>
                  <xsl:attribute name="rdf:resource">
                    <xsl:value-of select="'&type;unit-cm'" />
                  </xsl:attribute>
                </ecrm:P91_has_unit>
              </ecrm:E54_Dimension>
            </ecrm:P40_observed_dimension>
          </xsl:for-each>
          <xsl:for-each select="shape|note">
            <ecrm:P3_has_note>
              <xsl:value-of select="." />
            </ecrm:P3_has_note>
          </xsl:for-each>
        </ecrm:E16_Measurement>
      </ecrm:P39i_was_measured_by>
    </xsl:for-each>
    <xsl:for-each select="note">
      <ecrm:P129i_is_subject_of>
        <samm:S54_Comment>
          <xsl:attribute name="rdf:about">
            <xsl:value-of select="concat($uri, '-note-@@@hash11:', string(.), ':hsah@@@')" />
          </xsl:attribute>
          <ecrm:P3_has_note>
            <xsl:value-of select="." />
          </ecrm:P3_has_note>
        </samm:S54_Comment>
      </ecrm:P129i_is_subject_of>
    </xsl:for-each>
    <xsl:for-each select="provenance">
      <ecrm:P24i_changed_ownership_through>
        <samm:S8_Acquisition>
          <xsl:attribute name="rdf:about">
            <xsl:value-of select="concat($uri, '-prov-@@@hash11:', string(.), ':hsah@@@')" />
          </xsl:attribute>
          <xsl:for-each select="organisation">
            <ecrm:P23_transferred_title_from>
              <samm:S86_Organisation>
                <xsl:attribute name="rdf:about">
                  <xsl:value-of select="concat('&inst;org-@@@hash11:', string(.), ':hsah@@@')" />
                </xsl:attribute>
                <ecrm:P1_is_identified_by>
                  <ecrm:E41_Appellation>
                    <xsl:attribute name="rdf:about">
                      <xsl:value-of select="concat('&inst;org-@@@hash11:', string(.), ':hsah@@@-name')" />
                    </xsl:attribute>
                    <ecrm:P3_has_note>
                      <xsl:value-of select="." />
                    </ecrm:P3_has_note>
                  </ecrm:E41_Appellation>
                </ecrm:P1_is_identified_by>
              </samm:S86_Organisation>
            </ecrm:P23_transferred_title_from>
          </xsl:for-each>
          <xsl:for-each select="note">
            <ecrm:P129i_is_subject_of>
              <samm:S54_Comment>
                <xsl:attribute name="rdf:about">
                  <xsl:value-of select="concat($uri, '-prov-@@@hash11:', string(.), ':hsah@@@-note')" />
                </xsl:attribute>
                <ecrm:P3_has_note>
                  <xsl:value-of select="." />
                </ecrm:P3_has_note>
              </samm:S54_Comment>
            </ecrm:P129i_is_subject_of>
          </xsl:for-each>
        </samm:S8_Acquisition>
      </ecrm:P24i_changed_ownership_through>
    </xsl:for-each><!-- provenance -->
    <xsl:call-template name="ctt:digitized-image">
      <xsl:with-param name="bvid" select="$bvid" />
    </xsl:call-template>
  </samm:S1_Collection_Object>
</xsl:template>


<xsl:template match="literature">
  <xsl:param name="record" select="/record" />
  <xsl:variable name="uri">
    <xsl:choose>
      <xsl:when test="string(canonicalTitle) != ''">
        <xsl:value-of select="concat('&inst;lit-', translate(canonicalTitle, ' ,', '__'))" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="concat('&inst;lit-@@@hash11:', id, ':hsah@@@')" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <samm:S87_Literature>
    <xsl:attribute name="rdf:about">
      <xsl:value-of select="$uri" />
    </xsl:attribute>
    <xsl:if test="string(canonicalTitle) = ''">
      <ecrm:P2_has_type rdf:resource="&type;Literature" />
      <ecrm:P48_has_preferred_identifier>
        <samm:S100_Reference_Title>
          <xsl:attribute name="rdf:about">
            <xsl:value-of select="concat($uri, '-id')" />
          </xsl:attribute>
          <ecrm:P3_has_note>
            <xsl:value-of select="canonicalTitle" />
          </ecrm:P3_has_note>
        </samm:S100_Reference_Title>
      </ecrm:P48_has_preferred_identifier>
      <xsl:if test="title">
        <ecrm:P102_has_title>
          <ecrm:E35_Title>
            <xsl:attribute name="rdf:about">
              <xsl:value-of select="concat($uri, '-title')" />
            </xsl:attribute>
            <ecrm:P3_has_note>
              <xsl:value-of select="title" />
            </ecrm:P3_has_note>
          </ecrm:E35_Title>
        </ecrm:P102_has_title>
      </xsl:if>
      <!-- author and creation date -->
      <xsl:if test="author or date">
        <ecrm:P92i_was_brought_into_existence_by>
          <ecrm:E65_Creation>
            <xsl:attribute name="rdf:about">
              <xsl:value-of select="concat($uri, '-creation')" />
            </xsl:attribute>
            <!-- author -->
            <ecrm:P14_carried_out_by>
              <samm:S89_Author>
                <xsl:attribute name="rdf:about">
                  <xsl:value-of select="concat('&inst;person-@@@hash11:', author, ':hsah@@@')" />
                </xsl:attribute>
                <ecrm:P1_is_identified_by>
                  <ecrm:E41_Appellation>
                    <xsl:attribute name="rdf:about">
                      <xsl:value-of select="concat('&inst;person-@@@hash11:', author, ':hsah@@@-name')" />
                    </xsl:attribute>
                    <ecrm:P3_has_note>
                      <xsl:value-of select="author" />
                    </ecrm:P3_has_note>
                  </ecrm:E41_Appellation>
                </ecrm:P1_is_identified_by>
              </samm:S89_Author>
            </ecrm:P14_carried_out_by>
            <!-- date -->
            <xsl:for-each select="date">
              <ecrm:P4_has_time-span>
                <xsl:call-template name="ctt:date" />
              </ecrm:P4_has_time-span>
            </xsl:for-each>
          </ecrm:E65_Creation>
        </ecrm:P92i_was_brought_into_existence_by>
      </xsl:if>
    </xsl:if>
    <!-- the literature section / referenced part together with the references -->
    <ecrm:P106_is_composed_of>
      <samm:S101_Literature_Section>
        <xsl:variable name="sectionUri" select="concat('&inst;litsec-@@@hash11:', sectionId, ':hsah@@@')" />
        <xsl:attribute name="rdf:about">
          <xsl:value-of select="$sectionUri" />
        </xsl:attribute>
        <ecrm:P70_documents>
          <xsl:attribute name="rdf:resource">
            <xsl:value-of select="concat('&inst;work-', $record/work/bvid)" />
          </xsl:attribute>
        </ecrm:P70_documents>
        <xsl:for-each select="$record/itemList/item/shelfMark">
          <ecrm:P70_documents>
            <!-- add one link to each item -->
            <xsl:attribute name="rdf:resource">
              <xsl:value-of select="concat('&inst;item-', $record/work/bvid, '-@@@escapeUri:', ., ':epacse@@@')" />
            </xsl:attribute>
          </ecrm:P70_documents>
        </xsl:for-each>
        <!-- the identifier for the section, e.g. page number -->
        <xsl:if test="section/label">
          <ecrm:P1_is_identified_by>
            <samm:S102_Literature_Section_ID>
              <xsl:attribute name="rdf:about">
                <xsl:value-of select="concat($sectionUri, '-id')" />
              </xsl:attribute>
              <ecrm:P3_has_note>
                <xsl:value-of select="section" />
              </ecrm:P3_has_note>
            </samm:S102_Literature_Section_ID>
          </ecrm:P1_is_identified_by>
        </xsl:if>
        <!-- associated url -->
        <xsl:for-each select="url">
          <samm:N40_has_URL>
            <xsl:call-template name="ctt:compileUrl" />
          </samm:N40_has_URL>
        </xsl:for-each>
      </samm:S101_Literature_Section>
    </ecrm:P106_is_composed_of>
  </samm:S87_Literature>
</xsl:template>


<xsl:template match="person">
  <xsl:param name="record" select="/record" />
  <xsl:variable name="this" select="." />
  <ecrm:E21_Person>
    <xsl:variable name="uri" select="concat('&inst;person-@@@hash11:', id, ':hsah@@@')" />
    <xsl:attribute name="rdf:about">
      <xsl:value-of select="$uri" />
    </xsl:attribute>
    <xsl:for-each select="name">
      <ecrm:P1_is_identified_by>
        <ecrm:E41_Appellation>
          <xsl:attribute name="rdf:about">
            <xsl:value-of select="concat($uri, '-name-@@@hash7:', ., ':hsah@@@')" />
          </xsl:attribute>
          <ecrm:P3_has_note>
            <xsl:value-of select="." />
          </ecrm:P3_has_note>
        </ecrm:E41_Appellation>
      </ecrm:P1_is_identified_by>
    </xsl:for-each>    
    <xsl:apply-templates select="authority" />
    <xsl:for-each select="role[text() = 'author']">
      <xsl:for-each select="$record/itemList/item/shelfMark">
        <ecrm:P14i_performed>
          <ecrm:E12_Production>
            <xsl:attribute name="rdf:about">
              <xsl:call-template name="createItemUri">
                <xsl:with-param name="bvid" select="$record/work/bvid" />
                <xsl:with-param name="shelfMark" select="." />
              </xsl:call-template>
              <xsl:text>-production</xsl:text>
            </xsl:attribute>
          </ecrm:E12_Production>
        </ecrm:P14i_performed>
      </xsl:for-each>
    </xsl:for-each>
    <xsl:for-each select="role[text() = 'printer']">
      <xsl:for-each select="$record/itemList/item/shelfMark">
        <ecrm:P14i_performed>
          <ecrm:E12_Production>
            <xsl:attribute name="rdf:about">
              <xsl:call-template name="createItemUri">
                <xsl:with-param name="bvid" select="$record/work/bvid" />
                <xsl:with-param name="shelfMark" select="." />
              </xsl:call-template>
              <xsl:text>-production</xsl:text>
            </xsl:attribute>
          </ecrm:E12_Production>
        </ecrm:P14i_performed>
      </xsl:for-each>
    </xsl:for-each>
    <xsl:for-each select="role[text() = 'inventor']">
      <ecrm:P14i_performed>
        <ecrm:E65_Creation>
          <xsl:attribute name="rdf:about">
            <xsl:value-of select="concat('&inst;work-', $record/work/bvid, '-creation')" />
          </xsl:attribute>
        </ecrm:E65_Creation>
      </ecrm:P14i_performed>
    </xsl:for-each>
    <xsl:for-each select="role[text() = 'inventor' or text() = 'author' or text() = 'unspecified']">
      <grasa:had_influence_according_to>
        <grasa:Influence_Assignment>
          <xsl:attribute name="rdf:about">
            <xsl:value-of select="concat('&inst;work-', $record/work/bvid, '-creation-influence-@@@hash7:', $this/id, '::', ., ':hsah@@@')" />
          </xsl:attribute>
          <ecrm:P2_has_type>
            <ecrm:E55_Type>
              <xsl:attribute name="rdf:about">
                <xsl:value-of select="concat('&type;role-', .)" />
              </xsl:attribute>
            </ecrm:E55_Type>
          </ecrm:P2_has_type>
          <grasa:recorded_influence_upon>
            <ecrm:E65_Creation>
              <xsl:attribute name="rdf:about">
                <xsl:value-of select="concat('&inst;work-', $record/work/bvid, '-creation')" />
              </xsl:attribute>
            </ecrm:E65_Creation>
          </grasa:recorded_influence_upon>
        </grasa:Influence_Assignment>
      </grasa:had_influence_according_to>
    </xsl:for-each>
  </ecrm:E21_Person>
</xsl:template>

<xsl:template match="sequel">
  <xsl:param name="record" select="/record" />
  <xsl:param name="workUri" select="concat('&inst;work-', $record/work/bvid)" />
  <xsl:variable name="uri" select="concat('&inst;sequel-', bvid)" />
  <grasa:Sequel>
    <xsl:attribute name="rdf:about">
      <xsl:value-of select="$uri" />
    </xsl:attribute>
    <ecrm:P48_has_preferred_identifier>
      <grasa:BV-ID>
        <xsl:attribute name="rdf:about">
          <xsl:value-of select="concat($uri, '-id')" />
        </xsl:attribute>
        <ecrm:P3_has_note>
          <xsl:value-of select="bvid" />
        </ecrm:P3_has_note>
      </grasa:BV-ID>
    </ecrm:P48_has_preferred_identifier>
    <ecrm:P1_is_identified_by>
      <xsl:attribute name="rdf:resource">
        <xsl:value-of select="concat($uri, '-id')" />
      </xsl:attribute>
    </ecrm:P1_is_identified_by>
    <ecrm:P102_has_title>
      <ecrm:E35_Title>
        <xsl:attribute name="rdf:about">
          <xsl:value-of select="concat($uri, '-title')" />
        </xsl:attribute>
        <ecrm:P3_has_note>
          <xsl:value-of select="title" />
        </ecrm:P3_has_note>
      </ecrm:E35_Title>
    </ecrm:P102_has_title>
    <ecrm:P106_is_composed_of>
      <xsl:attribute name="rdf:resource">
        <xsl:value-of select="$workUri" />
      </xsl:attribute>
    </ecrm:P106_is_composed_of>
    <grasa:has_sequel_part>
      <xsl:attribute name="rdf:resource">
        <xsl:value-of select="$workUri" />
      </xsl:attribute>
    </grasa:has_sequel_part>
    <grasa:has_sequel_member>
      <grasa:Sequel_Member>
        <xsl:attribute name="rdf:about">
          <xsl:value-of select="concat($workUri, '-sequel-member')" />
        </xsl:attribute>
        <grasa:has_sequel_member_topic>
          <xsl:attribute name="rdf:resource">
            <xsl:value-of select="$workUri" />
          </xsl:attribute>        
        </grasa:has_sequel_member_topic>
        <xsl:for-each select="position">
          <grasa:has_sequel_position><xsl:value-of select="." /></grasa:has_sequel_position>
        </xsl:for-each>
      </grasa:Sequel_Member>
    </grasa:has_sequel_member>
  </grasa:Sequel>
</xsl:template>

</xsl:stylesheet>


