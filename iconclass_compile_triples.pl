#!/usr/bin/perl -w -CS
#
# Syntax: $0 <iconclass table as csv>  > <iconclass triples as nt>
# 
# outputs ntriples that relate object with iconclass term

# CSV notes:
# - columns: bv number, signature, creator, title, iconclass terms
# - no header in first row!
# - the columns can contain multilines, in which case they are encapsulated by quotes.
#   In fact, the iconclass terms are in one cell, separated by newline!
#

unless (scalar @ARGV == 1) {
  print "Syntax: $0 <iconclass table as csv>\n";
  exit 1;
}

use Text::CSV;
use URI::Escape;

$line = 0;
$csv = Text::CSV->new({ binary => 1, sep_char => "," });
print STDERR "reading from $ARGV[0]\n";
open $f, "<$ARGV[0]" or die "$!";
while ($fields = $csv->getline($f)) {
  print STDERR "processing line: " . ++$line . "\n";
  # extract bv number and iconclass ids
  $bv = $fields->[0];
  chomp $bv;
  if ($bv !~ /^BV\d+$/) {
    print STDERR "bad BV:$bv\n";
    next;
  }
  @classes = ();
  foreach $_ (split /\n/, $fields->[4]) {
    chomp;
    if( s/^\s*([0-9][0-9A-Z]*(\([-_0-9A-ZÄÖÜ.+ ]+\)[0-9A-Z]*)*).*$/$1/) {
      print STDERR "accept:$_\n";
      push @classes, $_;
    } else {
      print STDERR "reject:$_\n";
    }
  }
  # create triples
  $curi = "http://id.ub.fau.de/hgs/inst/work-$bv";
  foreach my $c (@classes) {
    $muri = "http://id.ub.fau.de/hgs/inst/item-$bv-motiv-" . uri_escape($c);
    $iuri = "http://iconclass.org/" . uri_escape($c);
    print "<$curi> <http://erlangen-crm.org/170309/P106_is_composed_of> <$muri> .\n";
    print "<$muri> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://objekte-im-netz.fau.de/ontology/graphik/Representation> .\n";
    print "<$muri> <http://objekte-im-netz.fau.de/ontology/graphik/is_classified_by_iconclass_as> <$iuri> .\n";
    print "\n";
  }
  print "\n";
}

