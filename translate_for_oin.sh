#!/bin/bash

for s in $@; do
  t=`basename $s`
  b=`echo "$t" | perl -pe 's/\.\d+(?:T\d+)?(\.[^.]+)$/$1/;'`
  echo "copying $t to /var/www/html/oin/$b"
  perl -pe 's!id.ub.fau.de/hgs/(inst|types)!objekte-im-netz.fau.de/graphik/id!g;' $s > /var/www/html/oin/$b
done
