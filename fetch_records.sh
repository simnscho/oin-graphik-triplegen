#!/bin/bash

echo "start fetching records at " `date`
c=0
for bvid in $(cat -); do
  ((c++))
  if [ "x$bvid" != "x" ]; then
    echo "read line $c with record $bvid"
    curl "http://bvbr.bib-bvb.de:8991/aleph-cgi/oai/oai_opendata.pl?verb=GetRecord&metadataPrefix=marc21&identifier=$bvid" > "data/source.b3kat/$bvid.xml"
    curl "http://uertp20.bib-bvb.de:8080/SRW/search/?query=dc.bvbid+%3D+%22$bvid%22&version=1.1&operation=searchRetrieve&recordSchema=info%3Asrw%2Fschema%2F1%2Fmarcxml-v1.1&maximumRecords=1&startRecord=1&resultSetTTL=300&recordPacking=xml" > "data/source.local/$bvid.xml"
  fi
done

echo "finished after $c lines"
