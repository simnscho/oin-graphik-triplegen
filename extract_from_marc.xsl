<?xml version="1.0" encoding="utf-8"?>

<!DOCTYPE bibtrip [
<!ENTITY inst "http://id.ub.fau.de/hgs/inst/">
]>

<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema"
  xmlns:srw="http://www.loc.gov/zing/srw/"
  xmlns:oai="http://www.openarchives.org/OAI/2.0/"
  xmlns:vmarc="http://www.loc.gov/MARC21/slim"
  xmlns:lmarc="http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd"
  xmlns:lmarc_srw="info:srw/schema/1/marcxml-v1.1"
  xmlns:xfm="http://id.ub.fau.de/bibtrip/extract-from-marc"
  xmlns:xfmlit="http://id.ub.fau.de/bibtrip/extract-from-marc-literature"
>

<!-- Transforms a pair of MARC files that describe an expression/work and corresponding items.
     Requires the pair of files as parameters "vdoc" and "ldoc".
-->

<xsl:import href="extract_from_marc_literature.xsl" />

<xsl:output method="xml" encoding="utf8" />

<xsl:param name="vdoc">
  <!-- filename of the MARC document from the BVB/KOBV
     as retrieved from http://bvbr.bib-bvb.de:8991/aleph-cgi/oai/oai_opendata.pl -->
</xsl:param>
<xsl:param name="ldoc">
  <!-- filename of the MARC document from the local system (Touchpoint SRW interface) -->
</xsl:param>



<xsl:variable name="v" select="document($vdoc)//vmarc:record" />
<xsl:variable name="l" select="document($ldoc)//lmarc_srw:record" />
<xsl:variable name="workId" select="$v/vmarc:controlfield[@tag='001']" />
<xsl:variable name="numberOfItems" select="count($l/lmarc:datafield[@tag='983'])" />


<xsl:template match="/">
  <xsl:call-template name="xfm:extract" />
</xsl:template>

<xsl:template name="xfm:extract">
  <record>
    <xsl:call-template name="xfm:extractWork" />
    <xsl:call-template name="xfm:extractItems" />
    <xsl:call-template name="xfm:extractLiterature" />
    <xsl:call-template name="xfm:extractPersons" />
    <xsl:call-template name="xfm:extractSequel" />
  </record>
</xsl:template>

<xsl:template name="xfm:extractItems">
  <itemList>
    <xsl:for-each select="$l/lmarc:datafield[@tag='983']">
      <xsl:variable name="shelfMark" select="current()/lmarc:subfield[@code='j']" />
      <item>
        <shelfMark><xsl:value-of select="$shelfMark" /></shelfMark>
        <title cleanse="yes">
          <xsl:value-of select="$v/vmarc:datafield[@tag='245']/vmarc:subfield[@code='a']" />
        </title>
        <subtitle cleanse="yes">
          <xsl:value-of select="$v/vmarc:datafield[@tag='245']/vmarc:subfield[@code='b']" />
        </subtitle>
        <technique>
          <xsl:value-of select="$v/vmarc:datafield[@tag='300']/vmarc:subfield[@code='b']" />
        </technique>
        <material><id>paper</id></material>
        <carries>
          <xsl:value-of select="$workId" />
        </carries>
        <creation>
          <xsl:call-template name="xfm:computeDate">
            <xsl:with-param name="date">
              <xsl:value-of select="$v/vmarc:datafield[@tag='264' and @ind1=' ' and @ind2='0']/vmarc:subfield[@code='c']" />
            </xsl:with-param>
          </xsl:call-template>
        </creation>
        <xsl:variable name="dims" select="tokenize($v/vmarc:datafield[@tag='300' and @ind1=' ' and @ind2=' ']/vmarc:subfield[@code='c'], ';')" />
        <xsl:for-each select="$dims">
          <xsl:variable name="dim" select="." />
          <xsl:if test="$dim != ''">
            <dimensions>
              <xsl:choose>
                <xsl:when test="contains(lower-case($dim), 'rund') or contains(lower-case($dim), 'durchmesser') or contains($dim, '⌀')">
                  <diameter>
                    <xsl:value-of select="replace($dim, '^.*?(\d+(,\d+)?).*$', '$1') "/>
                  </diameter>
                  <shape>rund</shape>
                </xsl:when>
                <xsl:when test="contains($dim, 'x')">
                  <height>
                    <xsl:value-of select="replace($dim, '^.*?(\d+(,\d+)?).*x.*$', '$1') "/>
                  </height>
                  <width>
                    <xsl:value-of select="replace($dim, '^.*x\s*(\d+(,\d+)?).*$', '$1') "/>
                  </width>
                  <xsl:if test="contains(lower-case($dim), 'oval')">
                    <shape>oval</shape>
                  </xsl:if>
                </xsl:when>
              </xsl:choose>
              <xsl:if test="matches($dim, '\(.+\)')">
                <note>
                  <xsl:value-of select="replace($dim, '^.*\((.+)\).*$', '$1')" />
                </note>
              </xsl:if>
            </dimensions>
          </xsl:if>
        </xsl:for-each>
        <xsl:for-each select="lmarc:subfield[@code='n']">
          <xsl:call-template name="xfm:extractNoteAndProvenance" />
        </xsl:for-each>
      </item>
    </xsl:for-each>
  </itemList>
</xsl:template>


<xsl:template name="xfm:extractNoteAndProvenance">
  <xsl:param name="string" select="string(.)" />
  <xsl:choose>
    <xsl:when test="contains($string, 'Provenienz:')">
      <xsl:if test="normalize-space(substring-before($string, 'Provenienz:')) != ''">
        <note>
          <xsl:value-of select="normalize-space(substring-before($string, 'Provenienz:'))" />
        </note>
      </xsl:if>
      <xsl:choose>
        <xsl:when test="contains($string, 'Ansbach')">
          <provenance><organisation>Graphische Sammlung der Markgrafen von Ansbach</organisation></provenance>
          <xsl:if test="normalize-space(substring-after($string, 'Ansbach')) != ''">
            <note>
              <xsl:value-of select="normalize-space(substring-after($string, 'Ansbach'))" />
            </note>
          </xsl:if>
        </xsl:when>
        <xsl:otherwise>
          <provenance><note><xsl:value-of select="normalize-space(substring-after($string, 'Provenienz:'))" /></note></provenance>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:otherwise>
      <note>
        <xsl:value-of select="$string" />
      </note>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<xsl:template name="xfm:extractWork">
  <work>
    <bvid>
      <xsl:value-of select="$workId" />
    </bvid>
    <title cleanse="yes">
      <xsl:value-of select="$v/vmarc:datafield[@tag='245']/vmarc:subfield[@code='a']" />
    </title>
    <subtitle cleanse="yes">
      <xsl:value-of select="$v/vmarc:datafield[@tag='245']/vmarc:subfield[@code='b']" />
    </subtitle>
    <xsl:for-each select="$v/vmarc:datafield[@tag='240']/vmarc:subfield[@code='0']">
      <xsl:call-template name="xfm:compileAuthorityEntry">
        <xsl:with-param name="marcAuthStr" select="." />
      </xsl:call-template>
    </xsl:for-each>
    <production>
      <xsl:call-template name="xfm:computeDate">
        <xsl:with-param name="date" select="$v/vmarc:datafield[@tag='264' and @ind1=' ' and @ind2='0']/vmarc:subfield[@code='c']" />
      </xsl:call-template>
    </production>
  </work>
</xsl:template>


<xsl:template name="xfm:computeDate">
  <xsl:param name="date" />
  <date>
    <label>
      <xsl:value-of select="translate($date, '[]', '')" />
    </label>
    <xsl:choose>
      <xsl:when test="matches($date, '^\s*\[?\d{4}\]?\s*$')">
        <xsl:variable name="y" select="replace($date, '^.*(\d{4}).*$', '$1')" />
        <earliestStart>
          <xsl:value-of select="concat($y, '-01-01')" />
        </earliestStart>
        <latestStart />
        <earliestEnd />
        <latestEnd>
          <xsl:value-of select="concat($y, '-12-31')" />
        </latestEnd>
        <cleansed>
          <xsl:value-of select="translate($date, '[]', '')" />
        </cleansed>
      </xsl:when>
      <xsl:when test="matches($date, '^\s*\[?\d{4}\??\]?\s*$')">
        <xsl:variable name="y" select="number(replace($date, '^.*(\d{4}).*$', '$1'))" />
        <earliestStart>
          <xsl:value-of select="concat($y - 2, '-01-01')" />
        </earliestStart>
        <latestStart />
        <earliestEnd />
        <latestEnd>
          <xsl:value-of select="concat($y + 2, '-12-31')" />
        </latestEnd>
        <cleansed>
          <xsl:value-of select="translate($date, '[]', '')" />
        </cleansed>
      </xsl:when>
      <xsl:when test="matches($date, '^\s*\d\d\.?\s*(Jh|Jahrh).*\s*$')">
        <xsl:variable name="y" select="number(replace($date, '^.*(\d{2}).*$', '$1'))" />
        <earliestStart>
          <xsl:value-of select="concat($y - 1, '00-01-01')" />
        </earliestStart>
        <latestStart />
        <earliestEnd />
        <latestEnd>
          <xsl:value-of select="concat($y - 1, '99-12-31')" />
        </latestEnd>
        <cleansed>
          <xsl:value-of select="$date" />
        </cleansed>
      </xsl:when>
      <xsl:otherwise>
        <earliestStart />
        <latestStart />
        <earliestEnd />
        <latestEnd />
        <cleansed />
      </xsl:otherwise>
    </xsl:choose>
  </date>
</xsl:template>


<xsl:template name="xfm:compileAuthorityEntry">
  <xsl:param name="auth" />
  <xsl:param name="id" />
  <xsl:param name="marcAuthStr" />
  <xsl:variable name="auth0">
    <xsl:call-template name="xfm:getAuthFile">
      <xsl:with-param name="auth">
        <xsl:choose>
          <xsl:when test="$auth != ''">
            <xsl:value-of select="$auth" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="substring-before(substring-after($marcAuthStr, '('), ')')" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="id0">
    <xsl:choose>
      <xsl:when test="$id != ''">
        <xsl:value-of select="$id" />
      </xsl:when>
      <xsl:otherwise>
          <xsl:value-of select="substring-after($marcAuthStr, ')')" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <authority>
    <id><xsl:value-of select="concat($auth0/file/id, '::', $id0)" /></id>
    <xsl:copy-of select="$auth0" />
    <entry>
      <id>
        <xsl:value-of select="$id0" />
      </id>
      <xsl:variable name="entryUri">
        <xsl:call-template name="xfm:getAuthEntryUri">
          <xsl:with-param name="auth" select="$auth0/file/id" />
          <xsl:with-param name="id" select="$id0" />
        </xsl:call-template>
      </xsl:variable>
      <xsl:if test="$entryUri != ''">
        <uri>
          <xsl:value-of select="$entryUri" />
        </uri>
      </xsl:if>
    </entry>
  </authority>
</xsl:template>


<xsl:template name="xfm:getAuthFile">
  <xsl:param name="auth" />
  <file>
    <xsl:choose>
      <xsl:when test="$auth = 'DE-588'">
        <id>gnd</id>
      </xsl:when>
      <xsl:when test="$auth = 'BVB' or $auth = ''">
        <id>b3kat</id>
      </xsl:when>
      <xsl:otherwise>
        <id>@@@hash11:<xsl:value-of select="$auth" />:hsah@@@</id>
        <label><xsl:value-of select="$auth" /></label>
      </xsl:otherwise>
    </xsl:choose>
  </file>
</xsl:template>


<xsl:template name="xfm:getAuthEntryUri">
  <xsl:param name="auth" />
  <xsl:param name="id" />
  <xsl:choose>
    <xsl:when test="$auth = 'gnd'">http://d-nb.info/gnd/<xsl:value-of select="$id" /></xsl:when>
    <xsl:when test="$auth = 'b3kat'">http://gateway-bayern.de/<xsl:value-of select="$id" /></xsl:when>
    <xsl:otherwise />
  </xsl:choose>
</xsl:template>


<xsl:template name="xfm:extractLiterature">
  <xsl:variable name="info">
    <xsl:call-template name="xfmlit:getLiteratureInfo0" />
  </xsl:variable>
  <literatureList>
    <xsl:for-each select="$info/literature">
      <xsl:choose>
        <xsl:when test="./url and $info/literature[canonicalTitle = current()/canonicalTitle]/section">
          <!-- if this is an entry for a section URL and we have the same literature item again, 
               then skip this one and add the URL to the other one, so that we have only one entry.
               NB: URL entries have the <url> tag, non-URL entries have a <section> tag. so we can tell them apart! -->
        </xsl:when>
        <xsl:otherwise>
          <xsl:variable name="url">
            <xsl:choose>
              <xsl:when test="url">
                <xsl:value-of select="string(url)" />
              </xsl:when>
              <xsl:when test="$info/literature[canonicalTitle = current()/canonicalTitle]/url">
                <xsl:value-of select="string($info/literature[canonicalTitle = current()/canonicalTitle][1]/url)" />
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="''" />
              </xsl:otherwise>
            </xsl:choose>
          </xsl:variable>  
          <literature>
            <xsl:for-each select="*">
              <xsl:copy-of select="." />
            </xsl:for-each>
            <id>
              <xsl:choose>
                <xsl:when test="canonicalTitle != ''"><xsl:value-of select="canonicalTitle" /></xsl:when>
                <xsl:when test="author != '' or title != ''"><xsl:value-of select="concat(author, '::', title)" /></xsl:when>
                <xsl:otherwise><xsl:value-of select="''" /></xsl:otherwise>
              </xsl:choose>
            </id>
            <sectionId>
             <!-- <xsl:value-of select="concat($workId, '::')" />-->
              <xsl:choose>
                <xsl:when test="$url != ''"><xsl:value-of select="$url" /></xsl:when>
                <xsl:when test="canonicalTitle != '' and section != ''"><xsl:value-of select="concat(canonicalTitle, '::', section)" /></xsl:when>
                <xsl:when test="canonicalTitle != ''"><xsl:value-of select="canonicalTitle" /></xsl:when>
                <xsl:otherwise><xsl:value-of select="concat(author, '::', title, '::', section)" /></xsl:otherwise>
              </xsl:choose>
            </sectionId>
            <xsl:if test="not(url) and $url != ''">
              <url>
                <xsl:value-of select="$url" />
              </url>
            </xsl:if>
          </literature>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </literatureList>
</xsl:template>


<xsl:template name="xfm:extractPersons">
  <personList>
    <xsl:for-each select="$v/vmarc:datafield[(@tag='100' or @tag='700') and @ind1='1' and @ind2=' ']">
      <person>
        <xsl:if test="vmarc:subfield[@code='0']">
          <xsl:call-template name="xfm:compileAuthorityEntry">
            <xsl:with-param name="marcAuthStr" select="vmarc:subfield[@code='0']" />
          </xsl:call-template>
        </xsl:if>
        <name>
          <xsl:value-of select="vmarc:subfield[@code='a']" />
        </name>
        <xsl:for-each select="vmarc:subfield[@code='4' and text() = 'aut']">
          <role>author</role>
        </xsl:for-each>
        <xsl:for-each select="vmarc:subfield[@code='4' and text() = 'oth']">
          <role>unspecified</role>
        </xsl:for-each>
        <xsl:for-each select="vmarc:subfield[@code='4' and text() = 'inv']">
          <role>inventor</role>
        </xsl:for-each>
        <xsl:for-each select="vmarc:subfield[@code='4' and text() = 'pbl']">
          <role>publisher</role>
        </xsl:for-each>
        <xsl:for-each select="vmarc:subfield[@code='4' and text() = 'prt']">
          <role>printer</role>
        </xsl:for-each>
        <id>
          <xsl:choose>
            <xsl:when test="vmarc:subfield[@code='0']">
              <xsl:value-of select="vmarc:subfield[@code='0']" />
            </xsl:when>
            <xsl:when test="vmarc:subfield[@code='d']">
              <xsl:value-of select="concat(vmarc:subfield[@code='a'], ':', vmarc:subfield[@code='d'])" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="vmarc:subfield[@code='a']" />
            </xsl:otherwise>
          </xsl:choose>
        </id>
      </person>
    </xsl:for-each>
  </personList>
</xsl:template>


<xsl:template name="xfm:extractSequel">
  <xsl:for-each select="$v/vmarc:datafield[@tag='830']">
    <sequel>
      <!-- the bvid is prefixed with an institution id (sigel). we have to chop it off. -->
      <bvid><xsl:value-of select="substring-after(vmarc:subfield[@code='w'], ')')" /></bvid>
      <title cleanse="yes"><xsl:value-of select="vmarc:subfield[@code='a']" /></title>
      <xsl:for-each select="vmarc:subfield[@code='v']">
        <position><xsl:value-of select="." /></position>
      </xsl:for-each>
    </sequel>
  </xsl:for-each>
</xsl:template>

</xsl:stylesheet>


