#!/bin/bash

#rm data/xfm/*.xml

# read BV IDs from stdin, one per line
c=0
for bvid in $(cat -); do
  ((c++))
  if [ "x$bvid" != "x" ]; then
    echo "read $c lines; process id $bvid"
    java -jar /usr/share/java/Saxon-HE.jar -s:dummy.xml -xsl:extract_from_marc.xsl vdoc=data/source.b3kat/$bvid.xml ldoc=data/source.local/$bvid.xml |
    # cleanse elements tagged with cleanse="yes"
    php -r '
      while ($l = fgets(STDIN)) {
        $l = preg_replace_callback('\''!(\s*cleanse="yes">)([^<]*)!u'\'', function($matches) {
          return ">" . str_replace(["&lt;&lt;", "&gt;&gt;", "&#x98;", "&#x9c;"], "", $matches[2]);
        }, $l);
        echo $l;
      }' |
    xmlstarlet fo > data/xfm/$bvid.xml 
  fi
done
  
