#!/bin/bash

if [ "x$1" = "x" -o "x$2" = "x" ]; then
  echo "Syntax: $0 <excel input file> <csv output file>"
  exit 1;
fi

in2csv $1 | csvcut -c 1,2,3,4,5 -x | tail -n +2 > $2


