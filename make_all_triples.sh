#!/bin/bash

# Creates an object triples file and a separate literature triples file.
#
# Log everything to log_all_<DATE>.txt
#
# The script takes some time -- several hours.

#
# Expects list of BV numbers from stdin.
# Default lists for Bock Zeichnungen (bz) and Druckgraphiken (dr) can be found
# in id_list_(bz|dr).txt
# 
# How to create the list of BV numbers:
# 1. Go to EC Client.
# 2. Search for Lokaler Schlüssel dr
# 3. Print to file. Format can be e.g. ALPHAKAT
# 5. Log in to console on uerz2
# 6. Extract BV numbers from file
# 7. scp file into directory data/bvids
# 8. Repeat steps 2-6 for Lokaler Schlüssel bz, ...
#
# When all BV number files are on this machine, start the script:
# cat <BVno files>... | ./make_all_triples.sh
#

cd $(dirname $0)

DATE=`date "+%Y%m%dT%H%M%S"`
if [[ "x$1" = "x" ]]; then
  SUFFIX=""
else 
  SUFFIX="_$1"
fi
LOG="log_all_$DATE.txt"
echo "THE START: $DATE" > $LOG
echo "working in" `pwd` >> $LOG


# we get the list of all BV numbers from stdin
# we store them in a temporary file to pipe them into subcommands
cat - > $$.tmp

# create the data dirs if necessary
mkdir -p data/bvids data/ctt data/iconclass data/nt data/pid data/source.b3kat data/source.local data/xfm

# fetch all marc records from local system and b3kat
echo "start fetching records" >> $LOG
./fetch_all_data.sh < $$.tmp
echo "finished fetching records" >> $LOG

# for each bv number extract information from both b3kat and local marc files and compile a single xml
echo "start extracting from marc" >> $LOG
./make_xfm.sh < $$.tmp >> $LOG 2>&1
echo "finished extracting from marc" >> $LOG

# compile the xml to rdf/xml triples for each bv number
echo "==========" >> $LOG
echo "start compiling to triples" >> $LOG
./make_ctt.sh < $$.tmp >> $LOG 2>&1
echo "finished compiling to triples" >> $LOG

# convert the rdf/xml to ntriples and put them all in one big file
echo "==========" >> $LOG
echo "start making ntriples" >> $LOG
./make_nt.sh < $$.tmp > all$SUFFIX.$DATE.nt 2>> $LOG
echo "finished making ntriples" >> $LOG

# compile the literature works information into triples
# this information is manually compiled and rather static. nonetheless, we run the compilation
# everytime we update the normal data to be sure they match
echo "==========" >> $LOG
echo "compiling static triples" >> $LOG
./make_lit.pl < static_lit.tsv > static_lit.$DATE.nt 2>> $LOG

# make versions for oin with altered namespaces. copies them to /ub/hgs/web
echo "==========" >> $LOG
echo "translating for OIN" >> $LOG
./translate_for_oin.sh all$SUFFIX.$DATE.nt static_lit.$DATE.nt static_triples.rdf >> $LOG 2>&1

# move the created triple files to the data dir
mv all$SUFFIX.$DATE.nt static_lit.$DATE.nt data/nt

# remove the temporary file with the BV numbers
rm $$.tmp
# we are done
echo "==========" >> $LOG
echo "THE END:" `date "+%Y%m%dT%H%M%S"` >> $LOG

