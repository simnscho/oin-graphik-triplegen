#/bin/bash

while read -r LINE; do
  SIG=`echo "$LINE" | sed 's! !%20!g; s!(!%28!g; s!,!%2C!g; s!/!%2F!g; s!\[!%5B!g;'`
  echo "=== $LINE ==="
  URL="http://uertp20.bib-bvb.de:8080/SRW/search/?query=dc.anywhere%3D%22$SIG%22&version=1.1&operation=searchRetrieve&recordSchema=info%3Asrw%2Fschema%2F1%2Fmarcxml-v1.1&maximumRecords=10&startRecord=1&resultSetTTL=300&recordPacking=xml"
  curl -s $URL | xmlstarlet sel -N 'm=info:srw/schema/1/marcxml-v1.1' -t -m '//m:record' -v '*[@tag="035"]/*[@code="a"]' -n -o 'Autor: ' -v '*[@tag="700"]/*[@code="a"]' -n -o 'Titel: ' -v '*[@tag="245"]/*[@code="a"]' -n -n 
done
