#!/bin/bash

BVID_LIST=$(cat -)
echo "authors:"
for bvid in $BVID_LIST; do xmlstarlet sel -t -m '//*[@tag="787"]/*[@code="a"]' -v '.' -n data/source.b3kat/$bvid.xml; done | sort | uniq -c
echo "titles and volumes by author:"
for bvid in $BVID_LIST; do xmlstarlet sel -t -m '//*[@tag="787"]' -v '*[@code="a"]' -o ': ' -v '*[@code="t"]' -o '; Volg: ' -v '*[@code="g"]' -o '; Volg4: ' -v '*[@code="4"]' -n data/source.b3kat/$bvid.xml; done | sort | uniq -c

