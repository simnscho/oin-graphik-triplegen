<?xml version="1.0" encoding="utf-8"?>

<!DOCTYPE bibtrip [
<!ENTITY inst "http://id.ub.fau.de/hgs/inst/">
]>

<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema"
  xmlns:srw="http://www.loc.gov/zing/srw/"
  xmlns:oai="http://www.openarchives.org/OAI/2.0/"
  xmlns:vmarc="http://www.loc.gov/MARC21/slim"
  xmlns:lmarc="http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd"
  xmlns:lmarc_srw="info:srw/schema/1/marcxml-v1.1"
  xmlns:xfmlit="http://id.ub.fau.de/bibtrip/extract-from-marc-literature"
>

<!-- Helper methods for the extract_from_marc.xsl script that deals with extracting and
     cleansing the data in the literature sections.
     Operates on the vdoc data only.
-->


<xsl:template name="xfmlit:getLiteratureInfo0">
  <xsl:for-each select="$v/vmarc:datafield[@tag='787']">
    <xsl:variable name="litItems">
      <xsl:choose>
        <xsl:when test="count(vmarc:subfield[@code='g']) = 0">
          <item>
            <section/>
            <lit>
              <xsl:copy-of select="*" />
            </lit>
          </item>
        </xsl:when>
        <xsl:otherwise>
          <xsl:for-each select="vmarc:subfield[@code='g']">
            <item>
              <section>
                <xsl:value-of select="." />
              </section>
              <lit>
                <xsl:copy-of select="../*" />
              </lit>
            </item>
          </xsl:for-each>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:for-each select="$litItems/item">
      <literature>
        <author><xsl:value-of select="lit/vmarc:subfield[@code='a']" /></author>
        <title cleanse="yes"><xsl:value-of select="lit/vmarc:subfield[@code='t']" /></title>
        <section><xsl:value-of select="section" /></section>
        <canonicalTitle>
          <xsl:call-template name="xfmlit:getCanonicalTitle">
            <xsl:with-param name="author" select="lit/vmarc:subfield[@code='a']" />
            <xsl:with-param name="title" select="lit/vmarc:subfield[@code='t']" />
            <xsl:with-param name="section" select="section" />
          </xsl:call-template>
        </canonicalTitle>
      </literature>
    </xsl:for-each>
  </xsl:for-each>
  <xsl:for-each select="$v/vmarc:datafield[@tag='856' and (@ind2=' ' or @ind2='2') and not(contains(vmarc:subfield[@code='z'], 'Volltext'))]">
    <literature>
      <url><xsl:value-of select="vmarc:subfield[@code='u']" /></url>
      <canonicalTitle>
        <xsl:call-template name="xfmlit:getCanonicalTitle">
          <xsl:with-param name="url" select="vmarc:subfield[@code='u']" />
        </xsl:call-template>
      </canonicalTitle>
    </literature>
  </xsl:for-each>
  <xsl:for-each select="$v/vmarc:datafield[@tag='856' and (@ind2=' ' or @ind2='0' or @ind2='1') and contains(vmarc:subfield[@code='z'], 'Volltext')]">
    <!-- object entries in our own digital collections portal are treated like other literature -->
    <literature>
      <url>
        <xsl:value-of select="./vmarc:subfield[@code='u']" />
      </url>
      <canonicalTitle>Digitale Sammlung der UB</canonicalTitle>
    </literature>
  </xsl:for-each>
</xsl:template>


<xsl:template name="xfmlit:extractVolume">
  <xsl:param name="section" />
  <!-- a dot usually separates the volume from the page/item number;
       remove optional "Band" keyword and whitespace -->
  <xsl:variable name="s" select="replace(replace(substring-before($section, '.'), 'Band', ''), ' ', '')" />
  <xsl:choose>
    <!-- convert romans to arabic numbers;
         simple heuristics:
         if a number is 29 or less, it is assumed to be a volume number,
         if it is bigger it's a page number -->
    <xsl:when test="$s = 'I'">1</xsl:when>
    <xsl:when test="$s = 'II'">2</xsl:when>
    <xsl:when test="$s = 'III'">3</xsl:when>
    <xsl:when test="$s = 'IV'">4</xsl:when>
    <xsl:when test="$s = 'V'">5</xsl:when>
    <xsl:when test="$s = 'VI'">6</xsl:when>
    <xsl:when test="$s = 'VII'">7</xsl:when>
    <xsl:when test="$s = 'VIII'">8</xsl:when>
    <xsl:when test="$s = 'IX'">9</xsl:when>
    <xsl:when test="$s = 'X'">10</xsl:when>
    <xsl:when test="$s = 'XI'">11</xsl:when>
    <xsl:when test="$s = 'XII'">12</xsl:when>
    <xsl:when test="$s = 'XIII'">13</xsl:when>
    <xsl:when test="$s = 'XIV'">14</xsl:when>
    <xsl:when test="$s = 'XV'">15</xsl:when>
    <xsl:when test="$s = 'XVI'">16</xsl:when>
    <xsl:when test="$s = 'XVII'">17</xsl:when>
    <xsl:when test="$s = 'XVIII'">18</xsl:when>
    <xsl:when test="$s = 'XIX'">19</xsl:when>
    <xsl:when test="$s = 'XX'">20</xsl:when>
    <xsl:when test="$s = 'XXI'">21</xsl:when>	
    <xsl:when test="$s = 'XXII'">22</xsl:when>
    <xsl:when test="$s = 'XXIII'">23</xsl:when>
    <xsl:when test="$s = 'XXIV'">24</xsl:when>
    <xsl:when test="$s = 'XXV'">25</xsl:when>
    <xsl:when test="$s = 'XXVI'">26</xsl:when>
    <xsl:when test="$s = 'XXVII'">27</xsl:when>
    <xsl:when test="$s = 'XXVIII'">28</xsl:when>
    <xsl:when test="$s = 'XXIX'">29</xsl:when>
    <xsl:when test="number(substring($s, 1, 2)) > 29"><xsl:value-of select="''" /></xsl:when>
    <xsl:otherwise><xsl:value-of select="$s" /></xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="xfmlit:getCanonicalTitle">
  <xsl:param name="url" select="''"/>
  <xsl:param name="author" select="''"/>
  <xsl:param name="title" select="''"/>
  <xsl:param name="section" select="''"/>
  <xsl:variable name="volume">
    <xsl:call-template name="xfmlit:extractVolume">
      <xsl:with-param name="section" select="$section" />
    </xsl:call-template>
  </xsl:variable>
  <xsl:choose>
		<!-- urls / online media -->
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/andresen1872bd1')">Andresen 1</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/bartsch1802bd1')">Bartsch 1</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/bartsch1803bd2')">Bartsch 2</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/bartsch1803bd3')">Bartsch 3</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/bartsch1805bd4')">Bartsch 4</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/bartsch1805bd5')">Bartsch 5</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/bartsch1808bd6')">Bartsch 6</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/bartsch1808bd7')">Bartsch 7</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/bartsch1808bd8')">Bartsch 8</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/bartsch1808bd9')">Bartsch 9</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/bartsch1808bd10')">Bartsch 10</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/bartsch1808bd11')">Bartsch 11</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/bartsch1811bd12')">Bartsch 12</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/bartsch1811bd13')">Bartsch 13</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/bartsch1813bd14')">Bartsch 14</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/bartsch1813bd15')">Bartsch 15</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/bartsch1818bd16')">Bartsch 16</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/bartsch1818bd17')">Bartsch 17</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/bartsch1818bd18')">Bartsch 18</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/bartsch1819bd19')">Bartsch 19</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/bartsch1820bd20')">Bartsch 20</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/bartsch1821bd21')">Bartsch 21</xsl:when>
    <xsl:when test="contains($url, 'urn:nbn:de:bvb:29-bv009444368')">Bock</xsl:when>
    <xsl:when test="contains($url, 'urn:nbn:de:bvb:29-bv009445841')">Kessler-Luhde 1</xsl:when>
    <xsl:when test="contains($url, 'urn:nbn:de:bvb:29-bv009445843')">Kessler-Luhde 2,1</xsl:when>
    <xsl:when test="contains($url, 'urn:nbn:de:bvb:29-bv009445844')">Kessler-Luhde 2,2</xsl:when>
    <xsl:when test="contains($url, 'urn:nbn:de:bvb:29-bv009445845')">Kessler-Luhde 3</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/meder1932')">Meder</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/heller1827bd2_2')">Heller</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/passavant1860bd1')">Passavant 1</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/passavant1860bd2')">Passavant 2</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/passavant1862bd3')">Passavant 3</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/passavant1863bd4')">Passavant 4</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/passavant1864bd5')">Passavant 5</xsl:when>
    <xsl:when test="contains($url, 'digi.ub.uni-heidelberg.de/diglit/passavant1864bd6')">Passavant 6</xsl:when>
		<!-- end urls, begin print media -->
    <xsl:when test="contains($title, 'Altdeutsche Zeichnungen')">Altdeutsche Zeichnungen</xsl:when>
    <xsl:when test="contains($author, 'Andresen') and $volume = '1'">Andresen 1</xsl:when>
    <xsl:when test="contains($author, 'Andresen') and $volume = '2'">Andresen 2</xsl:when>
    <xsl:when test="contains($author, 'Andresen') and $volume = '3'">Andresen 4</xsl:when>
    <xsl:when test="contains($author, 'Andresen') and $volume = '4'">Andresen 5</xsl:when>
    <xsl:when test="contains($author, 'Bartsch') and $volume = '1'">Bartsch 1</xsl:when>
    <xsl:when test="contains($author, 'Bartsch') and $volume = '2'">Bartsch 2</xsl:when>
    <xsl:when test="contains($author, 'Bartsch') and $volume = '3'">Bartsch 3</xsl:when>
    <xsl:when test="contains($author, 'Bartsch') and $volume = '4'">Bartsch 4</xsl:when>
    <xsl:when test="contains($author, 'Bartsch') and $volume = '5'">Bartsch 5</xsl:when>
    <xsl:when test="contains($author, 'Bartsch') and $volume = '6'">Bartsch 6</xsl:when>
    <xsl:when test="contains($author, 'Bartsch') and $volume = '7'">Bartsch 7</xsl:when>
    <xsl:when test="contains($author, 'Bartsch') and $volume = '8'">Bartsch 8</xsl:when>
    <xsl:when test="contains($author, 'Bartsch') and $volume = '9'">Bartsch 9</xsl:when>
    <xsl:when test="contains($author, 'Bartsch') and $volume = '10'">Bartsch 10</xsl:when>
    <xsl:when test="contains($author, 'Bartsch') and $volume = '11'">Bartsch 11</xsl:when>
    <xsl:when test="contains($author, 'Bartsch') and $volume = '12'">Bartsch 12</xsl:when>
    <xsl:when test="contains($author, 'Bartsch') and $volume = '13'">Bartsch 13</xsl:when>
    <xsl:when test="contains($author, 'Bartsch') and $volume = '14'">Bartsch 14</xsl:when>
    <xsl:when test="contains($author, 'Bartsch') and $volume = '15'">Bartsch 15</xsl:when>
    <xsl:when test="contains($author, 'Bartsch') and $volume = '16'">Bartsch 16</xsl:when>
    <xsl:when test="contains($author, 'Bartsch') and $volume = '17'">Bartsch 17</xsl:when>
    <xsl:when test="contains($author, 'Bartsch') and $volume = '18'">Bartsch 18</xsl:when>
    <xsl:when test="contains($author, 'Bartsch') and $volume = '19'">Bartsch 19</xsl:when>
    <xsl:when test="contains($author, 'Bartsch') and $volume = '20'">Bartsch 20</xsl:when>
    <xsl:when test="contains($author, 'Bartsch') and $volume = '21'">Bartsch 21</xsl:when>
    <xsl:when test="contains($author, 'Bock')">Bock</xsl:when>
    <xsl:when test="contains($author, 'Dickel') and contains($title, 'Cranach')">Dickel Cranach</xsl:when>
    <xsl:when test="contains($author, 'Dickel') and contains($title, 'vor Dürer')">Dickel vor Duerer</xsl:when>
    <xsl:when test="contains($author, 'Dickel') and contains($title, 'seit Dürer')">Dickel seit Duerer</xsl:when>
    <xsl:when test="contains($author, 'Dodgson')">Dodgson</xsl:when>
    <xsl:when test="contains($author, 'Geisberg') and $volume = '1'">Geisberg 1</xsl:when>
    <xsl:when test="contains($author, 'Geisberg') and $volume = '2'">Geisberg 2</xsl:when>
    <xsl:when test="contains($author, 'Geisberg') and $volume = '3'">Geisberg 3</xsl:when>
    <xsl:when test="contains($author, 'Geisberg') and $volume = '4'">Geisberg 4</xsl:when>
    <xsl:when test="contains($author, 'Heller')">Heller</xsl:when>
    <xsl:when test="(contains($author, 'Hollstein') or contains($title, 'Hollstein')) and contains($title, 'engravings')">Hollstein</xsl:when>
    <xsl:when test="(contains($author, 'Luhde') or (contains($title, 'Druckgraphik') and contains($title, 'markgräflich') and contains($title, 'Besitz'))) and (contains($title, 'Band 1') or $volume = '1')">Kessler-Luhde 1</xsl:when>
    <xsl:when test="(contains($author, 'Luhde') or (contains($title, 'Druckgraphik') and contains($title, 'markgräflich') and contains($title, 'Besitz'))) and (contains($title, 'Band 2,1') or contains($volume, '2,1'))">Kessler-Luhde 2,1</xsl:when>
    <xsl:when test="(contains($author, 'Luhde') or (contains($title, 'Druckgraphik') and contains($title, 'markgräflich') and contains($title, 'Besitz'))) and (contains($title, 'Band 2,2') or contains($volume, '2,2'))">Kessler-Luhde 2,2</xsl:when>
    <xsl:when test="(contains($author, 'Luhde') or (contains($title, 'Druckgraphik') and contains($title, 'markgräflich') and contains($title, 'Besitz'))) and (contains($title, 'Band 3') or $volume = '3')">Kessler-Luhde 3</xsl:when>
    <xsl:when test="contains($author, 'Leonrodt')">Leonrodt</xsl:when>
    <xsl:when test="contains($author, 'Meder')">Meder</xsl:when>
    <xsl:when test="contains($author, 'Passavant') and $volume = '1'">Passavant 1</xsl:when>
    <xsl:when test="contains($author, 'Passavant') and $volume = '2'">Passavant 2</xsl:when>
    <xsl:when test="contains($author, 'Passavant') and $volume = '3'">Passavant 3</xsl:when>
    <xsl:when test="contains($author, 'Passavant') and $volume = '4'">Passavant 4</xsl:when>
    <xsl:when test="contains($author, 'Passavant') and $volume = '5'">Passavant 5</xsl:when>
    <xsl:when test="contains($author, 'Passavant') and $volume = '6'">Passavant 6</xsl:when>
    <xsl:when test="contains($author, 'Pinder')">Pinder</xsl:when>
    <xsl:when test="contains($author, 'Sachs')">Sachs</xsl:when>
    <xsl:when test="contains($author, 'Sallustius')">Sallustius</xsl:when>
    <xsl:when test="contains($author, 'Schoch')">Schoch</xsl:when>
    <xsl:when test="contains($author, 'Strauss') and contains($title, 'wood') and $volume = '1'">Strauss 1</xsl:when>
    <xsl:when test="contains($author, 'Strauss') and contains($title, 'wood') and $volume = '2'">Strauss 2</xsl:when>
    <xsl:when test="contains($author, 'Strauss') and contains($title, 'wood') and $volume = '3'">Strauss 3</xsl:when>
    <xsl:when test="contains($author, 'Strauss') and contains($title, 'Clair')">Strauss C</xsl:when>
    <xsl:when test="contains($author, 'Teget-Welz')">Teget-Welz</xsl:when>
    <xsl:when test="contains($title, 'geuerlicheiten') and contains($title, 'Ritters')">Tewrdannckh</xsl:when>
    <xsl:when test="contains($author, 'Maximilian') and contains($title, 'Kunig')">Weisskunig</xsl:when>
		<!-- end print media -->
    <xsl:otherwise><xsl:value-of select="''" /></xsl:otherwise>
  </xsl:choose>
</xsl:template>


<xsl:template name="xfmlit:canonicalNameToAuthId">
  <xsl:param name="name" select="." />
  <xsl:choose>
    <xsl:when test="$name = 'Dickel Cranach'">
      <bvb>BV044877608</bvb>
    </xsl:when>
    <xsl:when test="$name = 'Dickel seit Duerer'">
      <bvb>BV042061771</bvb>
    </xsl:when>
    <xsl:when test="$name = 'Dickel vor Duerer'">
      <bvb>BV037911044</bvb>
    </xsl:when>
    <xsl:when test="$name = 'Kessler-Luhde 1'">
      <bvb>BV009445841</bvb>
    </xsl:when>
    <xsl:when test="$name = 'Kessler-Luhde 2,1'">
      <bvb>BV009445843</bvb>
    </xsl:when>
    <xsl:when test="$name = 'Kessler-Luhde 2,2'">
      <bvb>BV009445844</bvb>
    </xsl:when>
    <xsl:when test="$name = 'Kessler-Luhde 3'">
      <bvb>BV009445845</bvb>
    </xsl:when>
    <xsl:when test="$name = 'Teget-Welz'">
      <bvb>BV044309678</bvb>
    </xsl:when>
    <xsl:when test="$name = 'Weisskunig'">
      <gnd>4343407-1</gnd>
    </xsl:when>
    <xsl:when test="$name = ''">
      <bvb></bvb>
      <viaf></viaf>
      <gnd></gnd>
      <wikidata></wikidata>
    </xsl:when>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>


