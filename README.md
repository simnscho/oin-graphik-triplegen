# oin-graphik-triplegen

Skripte zum Generieren der Triples für das Objekte-im-Netz-WissKI der Graphischen Sammlung der UB.

Die Skripte liegen im FAU-GitLab unter https://gitlab.rrze.fau.de/simnscho/oin-graphik-triplegen .

Die Skripte sind installiert auf dem Server dh1.ub.fau.de. 
Dieser Server ist nur innerhalb der FAU erreichbar!


# Konzept des Imports / der Triple-Generierung

## Verwendete Quellen

Die im OiN-Graphik-WissKI anzuzeigenden Informationen entstammen fünf verschiedenen Quellen, die
jeweils unterschiedliche Schnittstellen bieten:

- Dem bayrischen Verbundkatalog beziehungsweise dem B3Kat 
  (https://www.kobv.de/services/katalog/b3kat/) mit seiner OAI-PMH-Schnittstelle, welche Metadaten
  im MARCXML-Format zurückgibt.
  Hieraus werden die meisten Metadaten bezogen.
- Dem lokalen SISIS-Bibliothekssystem, das eine weitgehend undokumentierte SRW-Schnittstelle
  bereitstellt, welche ebenfalls MARCXML ausgibt.
  Die Schnittstelle ist weitgehend, aber nicht vollständig(!), SRU-kompatibel.
  (http://www.loc.gov/standards/sru/)
  Die verwendeten MARC-Felder unterscheiden sich leicht von denen der OAI-PMH-Schnittstelle.
  Aus dem Lokalsystem werden nur wenige Informationen bezogen, unter anderem aber die Signatur.
- Dem Multimedia-Server des BVB (Digitool), auf dem die Digitalisate liegen.
  Dieser bietet eine undokumentierte und inoffizielle(?) Webschnittstelle, um zu einer BV-Nummer
  die Digitalisat-ID zu ermitteln und die Bild-URL zu konstruieren.
- Einer Excel/CSV-Datei, die die Zuordnung von Iconclass-Termini zu BV-Nummern enthält.
  Diese liegt (bisher) nur lokal in der UB vor.
- Eine statitsche Triples-Datei mit generischen und allgemein gebrauchten Instanzen wie etwa
  für das festgelegte Material "Papier".


## Vorgehen beim Import in WissKI

Der Import in WissKI erfolgt durch Generierung von Triple-Dateien mittels der
Skripte und direkten Upload in den Triple Store. 
Nach dem Upload sind die Daten dann sofort in WissKI sicht- und nutzbar.

Folgende Dateien müssen (manuell) in den Triple Store geladen werden:

- all.nt: Erstellt aus Skript `make_all_triples.sh`
- static_lit.nt: Erstellt aus Skript `make_all_triples.sh`
- iconclass.nt: Erstellt aus Skript `iconclass_compile_triples.sh`
- static_triples.rdf: Statische Datei. Wird nicht erstellt.

Alle Dateien müssen für den Upload in den Triple Store im Webserver-Verzeichnis abgelegt werden
mittels des Skripts `translate_to_oin.sh`. (siehe unten)
Die Dateien sind dann über die URL `http://dh1.ub.fau.de/oin/<DATEI>` abrufbar.

Der Upload geschieht in der GUI von GraphDB unter Import > RDF.
Für jede Datei sollte dort bereits eine Vorlage zum (nochmaligen) Upload existieren. 
Die Dateien sollten in unterschiedliche Graphen importiert werden, um ein späteres Update zu
vereinfachen.
Momentan werden folgende Graphen verwendet:
- `http://objekte-im-netz.fau.de/graphik/id/objects-all`für `all.nt`
- `http://objekte-im-netz.fau.de/graphik/id/iconclass-taggings` für `iconclass.nt`
- `http://objekte-im-netz.fau.de/graphik/id/static` für `static_triples.rdf`


## Warum Skripte und Upload statt WissKI-ODBC-Import

Eine erste Variante des Imports verwendete das WissKI-Import-Modul, um die Daten aus den 
MARCXML-Dateien zu extrahieren und direkt in WissKI zu importieren.
Dieser Ansatz zeigte schnell einige Schwächen:
- Für die Extraktion und Aufbereitung der Daten aus dem MARCXML ist die XML-Funktionalität von
  MySQL zu begrenzt beziehungsweise manche Sachen lassen sich nur über (fragile) Umwege 
  realisieren. 
- Es müssen viele Gruppen und Pfade speziell und einzig für den Import angelegt werden, da
  die Datenstruktur in den MARC-Dateien für den WissKI-Import nicht optimal ist. 
- Es müssen viele kleine Importvorgänge (Gruppen) in festgelegter Reihenfolge konzertiert werden,
  da die Daten aus WissKI-Sicht häufig "verkehrt herum" vorhanden sind und Felder richtig
  disambiguiert werden. 
- Eine direkte Angabe von URIs ist nicht möglich, die Verlinkung kann nur über Disambiguierung
  erfolgen.
- Die Disambiguierung kommt hin und wieder durcheinander. Unter anderem, da die Metadaten teils
  nicht genormt sind oder Varianten aufweisen.
- Das Update von Objekt-Metadaten funktioniert nicht zuverlässig.

Daher wurde der Ansatz verworfen und diese Shell-Script-basierte Lösung gewählt, welche auf
geeignete Tools zurückgreifen kann:
- Die Skripte zur Extraktion aus den XML-Dateien und Erzeugung der Triples machen von XSLT 2.0
  Gebrauch und erfordern Xalan.
- Zur Datenaufbereitung und teils zur Triple-Generierung wird Perl genutzt

## Bekannte Probleme

- Iconclass-Codes mit Leerzeichen: Bei Iconclass werden die URIs direkt aus den Codes gebildet.
  Enthalten die Codes Leerzeichen wie bei 73D82(CROWN OF THORNS), werden diese mit %20 escaped.
  WissKI löst diese Escapes nicht richtig auf und kann diese Termini daher auch nicht darstellen.


# Anleitung zum Einsatz der Skripte und Erstellung der Triple-Dateien

Triple-Daten werden über den Aufruf zweier Skripte erstellt:

- `make_all_triples.sh`: Erstellt all die Triples, die zu einzelnen Objekten gehören.
- `iconclass_compile_triples.sh`: Erstellt die Verlinkungen zu Iconclass-Termen (Feld "Motiv")

Darüber hinaus gibt es das Skript `translate_to_oin.sh`, welches OiN-spezifische Anpassungen an
einer Triple-Datei vornimmt und diese im oin-Verzeichnis des Webservers ablegt, so dass die Datei
für den Upload in den Triple Store fertig ist.
Während `make_all_triples.sh` dieses Skript bereits ausführt, ist es auf die Ausgabe von 
`iconclass_compile_triples.sh` und auf die Datei `static_triples.rdf` anzuwenden.


## make_all_triples.sh

Diesem Skript wird eine Liste an BV-Nummern übergeben. 
Es erzeugt für die über die BV-Nummern spezifizierten Objekte alle relevanten Triples und 
speichert sie in einer Hand voll Dateien, die sodann in den Triple Store geladen werden können.

Als Ausgabe generiert das Skript Dateien im Verzeichnis data/nt:
- `all.\<ZEITSTEMPEL\>.nt`
- `static_lit.\<ZEITSTEMPEL\>.nt`
Im Webserver-Verzeichnis werden die Dateien ohne Zeitstempel im Namen abgelegt.
Zusätzlich sind diese Varianten weiter an das OiN-WissKI angepasst, zum Beispiel wird das
OiN-URI-Schema verwendet.

### Syntax

```$ cat <BVno files>... | ./make_all_triples.sh```

### Funktionsweise / Arbeitsablauf

Das Skript ruft sequentiell weitere Skripte auf, die
- die MARC-Datensätze aus dem Verbund(B3Kat)- und Lokalsystem herunterladen
  (`fetch_all_data.sh`, `fetch_records.sh`)
- die Objekt-zu-Bild-Verknüpfung aus Digitool ermitteln
  (`fetch_all_records.sh`, `get_image_pids.sh`)
- aus den beiden MARC-Datensätzen die benötigten Daten extrahieren und aufbereiten --- letzteres
  umfasst Encodings korrigieren, IDs/Hashes erzeugen, Datumswerte berechnen, Sonderfälle abfangen
  (`make_xfm.sh`, `extract_from_marc.xsl`, `extract_from_marc_literature.xsl`)
- aus den aufbereiteten Daten RDF-Dateien erzeugen --- eine Datei pro BV-Nummer
  (`make_ctt.sh`, `compile_to_triples.xsl`)
- die RDF-Dateien in eine große ntriples-Datei umwandeln
  (`make_nt.sh`)
- Triples aus den Literaturangaben der Datei `static_lit.tsv` kompilieren --- die Datei enthält
  genormte Angaben zu verwendeter Sekundärliteratur
  (`make_lit.pl`)
- die Triple-Dateien für den Import ins OiN-WissKI aufbereiten und im Webserver-Verzeichnis 
  platzieren, so dass der Import in den Triple Store reibungslos und immer gleich passieren kann.

Das Skript protokolliert den Lauf in die Datei `log_all_<ZEITSTEMPEL>.txt`.

# Datenverzeichnis

Das Skript und alle Unterskripte arbeiten auf dem Verzeichnis data.
Das Verzeichnis und alle Unterverzeichnisse werden erstellt, sollten sie nicht vorhanden sein.
Dort werden in den entsprechenden Verzeichnissen die Zwischenergebnisse abgelegt. 
Im Verzeichnis nt werden die finalen ntriples-Dateien abgelegt. 
(zum ntriples-Format siehe https://en.wikipedia.org/wiki/N-Triples)

### Liste BV-Nummern 

Die Liste der BV-Nummern kann entweder selbst erzeugt und auf dem Server gespeichert werden oder
es werden die vorhandenen `id_list_...` Dateien verwendet.

Um die Liste(n) selbst zu erzeugen, geht man wie folgt vor:
1. EC Client öffnen.
2. Im Feld "Lokaler Schlüssel" nach "dr" suchen.
3. In Datei drucken. Format kann zum Beispiel ALPHAKAT sein.
5. Über die Konsole auf uerz2 einloggen.
6. BV-Nummern aus der gedruckten Datei extrahieren.
   Zum Beispiel mit dem Befehl ```perl -i.bak -ne 'print if s/^.*(BV\d+).*$/$1/;' DRUCKDATEI```
7. Mit scp die Datei in das Verzeichnis data/bvids kopieren
8. Schritte 2-7 für weitere lokale Schlüssel wiederholen, wie bz, ...


## iconclass_compile_triples.sh

Die Triples zur Verlinkung der Objekte mit Iconclass werden separat zu den anderen Triples erzeugt.
Beide Skripte bedingen sich nicht.

Das Skript erwartet als Parameter eine CSV-Datei mit den Iconclass-Codes pro BV-Nummer.
Aus diesen Angaben erstellt es eine Triples-Datei, die dann in den Triple Store importiert werden
kann.

Das Skript protokolliert nach `STDERR` eventuell falsch formattierte BV-Nummern oder Iconclass-Codes.

### Das Format der CSV-Datei

Eine CSV-Datei findet sich unter `iconclass.csv`. 

Die CSV-Datei enthält mindestens 5 Spalten, wobei in der ersten Spalte die BV-Nummer hinterlegt 
ist und in der fünften Spalte ein oder mehrere Iconclass-Termini.
Die restlichen Spalten können beliebig belegt sein.
Eine Header-Zeile ist nicht vorhanden (erste Zeile mit Spaltenüberschriften).

In Spalte Fünf werden mehrere Termini durch Zeilenumbruch getrennt.
Am Anfang einer Zeile muss stets der Iconclass-Code stehen.
Getrennt durch Leerzeichen kann Freitext auf den Code folgen.

### Umwandeln einer Excel-Datei nach CSV.

Das Hilfsskript `iconclass_convert_excel.sh` wandelt eine Excel-Datei in eine CSV-Datei um.

Syntax: ```$ ./iconclass_convert_excel.sh <excel-Datei> <CSV-Datei>```


# Tabellarische Übersicht MARXCML und Felder in WissKI

Die Feld-Codes für die MARC-Spalten sind wie folgt zu verstehen:
1. 3x Field-Tag
2. 2x Indikator: `_` steht für ein Leerzeichen, ein `?` für ein beliebiges Zeichen
3. 1x Subfield-Code

| WissKI-Gruppe / -Feld | Aleph MARC | Sisis MARC | Kommentar |
|---|---|---|---|
| Sammlungsobjekt |  |  |  |
| . Bild | 856??u |  | Konstruktion der URL über die Digitool-PID; Extraktion der PID aus Feld 856u |
| . Inhalt |  |  |  |
| . . Titel | 245??a |  | 245b "Untertitel" wird momentan nicht in WissKI angezeigt |
| . Inhaltliche Schöpfung |  |  |  |
| . . Datierung Schöpfung | 264_0c |  | Transformation: |
| . . . Datum (informativ) |  |  | Feldinhalt wird direkt übernommen |
| . . . Frühestm. Startzeitpunkt |  |  | berechnete Angabe, wenn Feldinhalt bestimmten Mustern entspricht |
| . . . Spätestm. Endzeitpunkt |  |  | berechnete Angabe, wenn Feldinhalt bestimmten Mustern entspricht |
| . . Einflüsse | 1001_ und 7001_ |  | nur, wenn Subfield 4 eines aus "aut", "inv" oder "oth" ist |
| . . . Art des Einflusses | 1001_4 oder 7001_4 |  |  |
| . . . Künstler |  |  | Personen werden über Subfield 0, a+d oder nur a disambiguiert |
| . . . . Name | 1001_a oder 7001_a |  |  |
| . . . . Normdaten | 1001_0 oder 7001_0 |  | Dekomposition; s. ". Normdaten" |
| . Objektart | --- | --- | "Graphik" fest codiert |
| . Herstellung |  |  |  |
| . . Hersteller (Person) | 1001_ und 7001_ |  | nur, wenn Subfield 4 eines aus "aut" oder "prt" ist; Felder wie bei ". . Einflüsse" |
| . . Herstellungsdatum | 264_0c |  | s. ". Datierung Schöpfung" |
| . . Material | --- | --- | "Papier" fest codiert |
| . . Technik | 300??b |  |  |
| . Messung | 300__c |  | Dekomposition nach bestimmten Mustern in 300__c; Form (rund, oval, eckig) prinzipiell noch vorhanden, allerdings in WissKI kein Feld |
| . . Größe |  |  | Höhe, Breite oder Durchmesser |
| . . Wert |  |  |  |
| . . Einheit |  |  | immer cm |
| . Inventarnummer |  | 983??j |  |
| . BV-Nr. | 001 |  |  |
| . Normdaten | 240??0 |  | Dekomposition: |
| . . Normdatei |  |  | Extraktion bekannter Authority-Präfixe |
| . . Normdaten-ID |  |  | Abschneiden des Präfixes |
| . . URI |  |  | Konstruktion der URI bei bekannter Authority (GND) |
| . Provenienz |  |  | Dekomposition, normale Notizen in ". Bemerkung" |
| . . Vorbesitzer (Organisation) |  | 983??n | Nur wenn "Provenienz: xxx" vorhanden |
| . Literaturvermerk | 787?? und 856_2 |  | Über eine statische Tabelle von URL-, Titel-, Band- und Autormustern wird versucht, die analogen Werke mit den Digitalisaten zu verbinden |
| . . URL | 856_2u |  |  |
| . . Kurztitel | 856_2a, 856_2t und 856_2g |  | statische Tabelle von Kurztiteln, die mit den Feldern abgeglichen wird |
| . . Seite | 856_2g |  |  |
| . Bildinformation |  |  |  |
| . . URL | 856??u |  | Pfad wie "Sammlungsobjekt -> Bild" |
| . Bezeichnung/Titel |  |  | wie ". . Titel" |
| . Bemerkung |  | 983??n |  |



