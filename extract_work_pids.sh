#!/bin/bash
for bvid in $(cat -); do 
  xmlstarlet sel -t -m '//*[@tag="856" and contains(*[@code="z"], "Volltext")]' -v '*[@code="u"]' -n data/source.b3kat/$bvid.xml |
  grep -Po '(?<=pid=)\d+'
done |
sort -u
