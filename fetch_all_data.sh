#!/bin/bash

cd $(dirname $0)
echo  "working in" `pwd`

# we get the list of all BV numbers from stdin
cat - > $$.tmp

# fetch the b3cat record and the local record for each bv number
./fetch_records.sh < $$.tmp
# for each bv number get the corresponding pid for its scan in digitool
./get_image_pids.sh < $$.tmp

